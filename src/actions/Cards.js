import alt from '../alt';

class CardsActions {
  resolveCard(deck, id, tries) {
    return { deck, id, tries };
  }
}

export default alt.createActions(CardsActions);
