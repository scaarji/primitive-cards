import React from 'react';
import hotkey from 'react-hotkey';

hotkey.activate();

export default React.createClass({
  mixins: [hotkey.Mixin('handleHotkey')],

  handleHotkey(e) {
    if (e.keyCode === 32) {
      this.props.onNext();
    }
  },

  handleClick(e) {
    e.preventDefault();
    this.props.onNext();
  },

  render() {
    return (
      <div className="card-answer">
        <div className="card-text">{this.props.show}</div>
        <div className="card-text">
          <small>entered text</small>
          <span className={this.props.isCorrect ? 'card-answer-text correct' : 'card-answer-text incorrect'}>{this.props.candidate || '- none -'}</span>
        </div>
        <div className="card-text">
          <small>expected text</small>
          {this.props.expect}
        </div>
        <button className="card-btn" onClick={this.handleClick}>next</button>
      </div>
    );
  }
});
