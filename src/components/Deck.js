require('normalize.css');
require('styles/App.css');

import React from 'react';

import Card from './Card';

import TenseDeckStore from '../stores/TenseDeck';
import TranslationDeckStore from '../stores/TranslationDeck';

const DECKS = [TenseDeckStore, TranslationDeckStore];

export default React.createClass({
  render() {
    const deckName = this.props.params.deck;
    const matchingDecks = DECKS.filter(deck => deck.getName() === deckName);
    return matchingDecks.length ?
      (<Card deck={matchingDecks[0]} />) :
      (<div>Deck not found</div>);
  }
});
