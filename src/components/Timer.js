import React from 'react';

export default React.createClass({
  getInitialState() {
    return {
      timeLeft: this.props.time
    };
  },

  componentWillMount() {
    this.start();
  },

  componentWillUnmount() {
    this.stop();
  },

  start() {
    this.startedAt = Date.now();
    const updateInterval = this.props.updateInterval || 50;
    this.iid = setInterval(this.update, updateInterval);
  },

  stop() {
    clearInterval(this.iid);
  },

  update() {
    const timePassed = Date.now() - this.startedAt;
    const timeLeft = Math.max(0, this.props.time - timePassed);
    this.setState({
      timeLeft: timeLeft
    });

    if (timeLeft === 0) {
      this.stop();
      this.props.onFinish();
    }
  },

  render() {
    const percent = Math.round(100 * this.state.timeLeft / this.props.time);
    const lineStyle = { width: `${percent}%` }
    return (
      <div className="counter" style={lineStyle}></div>
    );
  }
})
