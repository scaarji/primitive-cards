require('styles/Card.css');

import React from 'react';
import _ from 'lodash';

import CardsActions from '../actions/Cards';

import CardsEmpty from './CardsEmpty';
import CardsQuestion from './CardsQuestion';
import CardsAnswer from './CardsAnswer';

export default React.createClass({
  getInitialState() {
    const cards = this.props.deck.getCards();

    return {
      cards,
      tries: new Map(),
      currentCard: _.first(cards),
      answer: null,
      isCorrect: false
    };
  },

  handleSubmit(candidate) {
    const currentCard = this.state.currentCard;

    const updatedCards = this.state.cards.slice(1);
    const isCorrect = candidate.toLowerCase().trim() === currentCard.expect;
    const currentTries = (this.state.tries.get(currentCard.id) || 0) + 1;

    if (!isCorrect) {
      updatedCards.push(currentCard);
    }

    this.setState({
      cards: updatedCards,
      answer: candidate,
      isCorrect: isCorrect,
      tries: this.state.tries.set(currentCard.id, currentTries)
    });

    if (isCorrect) {
      const deckName = this.props.deck.getName();

      CardsActions.resolveCard(deckName, currentCard.id, currentTries);
    }
  },

  handleNext() {
    this.setState({
      currentCard: _.first(this.state.cards),
      answer: null
    });
  },

  render() {
    let content = '';
    const currentCard = this.state.currentCard;
    if (!currentCard) {
      content = (<CardsEmpty />);
    } else {
      if (this.state.answer !== null) {
        content = (<CardsAnswer show={currentCard.show} expect={currentCard.expect} candidate={this.state.answer} onNext={this.handleNext} isCorrect={this.state.isCorrect}/>);
      } else {
        content = (<CardsQuestion show={currentCard.show} onSubmit={this.handleSubmit} />);
      }
    }

    return (
      <div className="card">{content}</div>
    );
  }
});
