require('styles/DeckList.css');
import React from 'react';

import { Link } from 'react-router'

import TenseDeckStore from '../stores/TenseDeck';
import TranslationDeckStore from '../stores/TranslationDeck';

const DECKS = [TenseDeckStore, TranslationDeckStore];

export default React.createClass({
  render() {
    return (
      <ul className="deck-list">
        {DECKS.map((deck) => (
          <li key={deck.getName()} className="deck-list-element">
            <Link to={`/decks/${deck.getName()}`} className="deck-list-element-link">{deck.getDescription()}</Link>
          </li>
        ))}
      </ul>
    );
  }
});
