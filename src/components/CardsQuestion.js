import React from 'react';

import Timer from './Timer';

export default React.createClass({
  handleSubmit() {
    this.props.onSubmit(this.refs.candidate.value);
  },

  onSubmit(e) {
    e.preventDefault();
    this.handleSubmit();
  },

  onTimeout() {
    this.handleSubmit();
  },

  componentDidMount() {
    this.refs.candidate.focus();
  },

  render() {
    return (
      <div className="card-question">
        <Timer time={10000} onFinish={this.onTimeout} />
        <div className="card-text">{this.props.show}</div>
        <div className="card-expect-form">
          <form onSubmit={this.onSubmit}>
            <input className="card-expect-input" type="text" ref="candidate" placeholder="answer" />
            <button className="card-btn" type="submit">send</button>
          </form>
        </div>
      </div>
    );
  }
});
