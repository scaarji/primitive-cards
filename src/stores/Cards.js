import alt from '../alt';

import CARDS from '../cards';

class CardsStore {
  constructor() {
    this.cards = CARDS.slice();

    this.exportPublicMethods({
      getCards: this.getCards
    });
  }

  getCards() {
    return this.state.cards.slice();
  }
}

export default alt.createStore(CardsStore, 'CardsStore');
