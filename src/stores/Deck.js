import CardsStore from './Cards';
import CardsActions from '../actions/Cards';

import date from '../helpers/date';

export default class DeckStore {
  get from() { throw new Error('Not implemented'); }
  get to() { throw new Error('Not implemented'); }
  get name() { return `${this.from}-${this.to}`; }
  get description() { throw new Error('Not implemented'); }

  constructor() {
    this.cards = this.mapCards(CardsStore.getCards());

    this.bindListeners({
      handleResolvedCard: CardsActions.RESOLVE_CARD
    });

    this.exportPublicMethods({
      getCards: this.getCards,
      getName: this.getName,
      getDescription: this.getDescription
    });
  }

  getCards() {
    return this.state.cards.slice();
  }

  getName() {
    return this.state.name;
  }

  getDescription() {
    return this.state.description;
  }

  _getCard(id) {
    return this.cards
      .reduce((found, card) => card.id === id ? card : found, null);
  }

  handleResolvedCard({ deck, id, tries }) {
    if (deck !== this.name) return;

    const card = this._getCard(id);
    card.showedOn.push(Date.now());
    card.tries.push(tries);
    card.showOn = this._calculateShowOn(card);

    this.updateCardInfo(card);
  }

  _calculateShowOn(card) {
    const lastTries = card.tries[card.tries.length - 1];

    const shownOnce = card.showedOn.length === 1;
    const onFirstTry = lastTries === 1;
    if (shownOnce || !onFirstTry) {
      return date.tomorrow();
    } else {
      return date.next(card.showedOn);
    }
  }

  cardKey(card) {
    return `${this.name}:${card.id}`;
  }

  getCardInfo(card) {
    const key = this.cardKey(card);
    const infoString = localStorage.getItem(key);
    let info = {};
    if (infoString) {
      try {
        info = JSON.parse(infoString);
      } catch (err) {
        localStorage.removeItem(key);
        info = {};
      }
    }

    return info;
  }

  updateCardInfo(card) {
    const key = this.cardKey(card);
    const infoString = JSON.stringify({
      showOn: card.showOn,
      showedOn: card.showedOn,
      tries: card.tries
    });
    localStorage.setItem(key, infoString);
  }

  populateCard(card) {
    const info = this.getCardInfo(card);

    return {
      id: card.id,
      show: card.show,
      expect: card.expect,
      showOn: info.showOn || 0,
      showedOn: info.showedOn || [],
      tries: info.tries || []
    };
  }

  filterCard() {
    return true;
  }

  mapCard(card) {
    return {
      id: `${card[this.from]}:${card[this.to]}`,
      show: card[this.from],
      expect: card[this.to]
    };
  }

  mapCards(cards) {
    const now = Date.now();

    return cards
      .filter(card => this.filterCard(card))
      .map(card => this.mapCard(card))
      .map(card => this.populateCard(card))
      .filter(card => card.showOn < now)
      .sort((a, b) => a.showOn - b.showOn);
  }
}
