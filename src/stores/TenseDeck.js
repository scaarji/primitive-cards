import alt from '../alt';
import DeckStore from './Deck';

class TenseDeckStore extends DeckStore {
  get from() { return 'de'; }
  get to() { return 'perfect'; }

  get description() { return 'DE verbs from to perfect forms'; }

  filterCard(card) {
    return card.type === 'verb';
  }
}

export default alt.createStore(TenseDeckStore, 'TenseDeckStore');
