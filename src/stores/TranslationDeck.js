import alt from '../alt';
import DeckStore from './Deck';

class TranslationDeckStore extends DeckStore {
  get from() { return 'en'; }
  get to() { return 'de'; }

  get description() { return 'From EN to DE'; }
}

export default alt.createStore(TranslationDeckStore, 'TranslationDeckStore');
