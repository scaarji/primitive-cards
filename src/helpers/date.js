
export default {
  day: 24 * 60 * 60 * 1000,
  periods: [1, 4, 20, 100],

  daysFromToday(cnt) {
    const date = new Date();
    date.setDate(date.getDate() + cnt);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    return date.getTime();
  },

  tomorrow() {
    return this.daysFromToday(1);
  },

  next(showedOn) {
    const lastShownOn = showedOn[showedOn.length - 1];
    const previousShownOn = showedOn[showedOn.length - 2];

    const diff = Math.round((lastShownOn - previousShownOn) / this.day);
    const period = this.periods.find(period => period > diff);

    return this.daysFromToday(period);
  }
};
