require('normalize.css');
require('styles/App.css');

import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router'

import Deck from './components/Deck';
import DeckList from './components/DeckList';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={DeckList} />
    <Route path="/decks/:deck" component={Deck} />
  </Router>
), document.getElementById('app'));
